import Link from 'next/link'
import { ProductCard } from '../../components/antd/product-card'
import { IconPets, IconRestaurant, IconStore } from '../../components/IconCustom'

export default function Home() {
  return (
    <>
      {/*  */}
      <div className="relative -top-3 md:w-[750px] xl:w-[1170px] mx-auto">
        {/* banner */}
        <div className="flex py-2 bg-white divide-x divide-gray-200 rounded-lg shadow-lg "> 
            <div className="flex items-center justify-around w-1/3 px-10 py-3 text-gray-800">
                <IconStore className="w-16 h-16 fill-sky-500" />
                <div>
                  <p className="mb-1 text-4xl font-black "> {20}  </p>
                  <p className="text-lg"> Cửa hàng </p>
                </div> 
            </div>
            <div className="flex items-center justify-around w-1/3 px-10 py-3 text-gray-800">
                <IconPets className="w-16 h-16 fill-yellow-500" />
                <div>
                  <p className="mb-1 text-4xl font-black "> {50} + </p>
                  <p className="text-lg"> Thú nuôi </p>
                </div> 
            </div>
            <div className="flex items-center justify-around w-1/3 px-10 py-3 text-gray-800">
                <IconRestaurant className="w-16 h-16 fill-rose-400" />
                <div>
                  <p className="mb-1 text-4xl font-black "> {99}+  </p>
                  <p className="text-lg"> Sản Phẩm </p>
                </div> 
            </div>
        </div>
        {/* banner */}
        <div className="grid grid-cols-3 gap-3 mt-10 shadow">
          <img src='/images/banner-2.png' alt='b1' className="object-cover object-center"/>
          <img src='/images/banner-3.png' alt='b2' className="object-cover object-center"/>
          <img src='/images/banner-4.png' alt='b3' className="object-cover object-center"/>
        </div>
        <ProductCard header='Danh sách sản phẩm bán chạy' viewMoreText='Làm mới'/>
        {/* banner */}
        <div className="grid grid-cols-3 gap-3 mt-10 shadow">
          <img src='/images/banner-3.png' alt='b2' className="object-cover object-center"/>
          <img src='/images/banner-4.png' alt='b3' className="object-cover object-center"/>
          <img src='/images/banner-2.png' alt='b1' className="object-cover object-center"/>
        </div>
        <ProductCard header='Danh sách sản phẩm bán chạy' viewMoreText='Làm mới'/>
        <div className='mt-10 text-center'>
          <img src='/images/banner-5.png' alt='b5'  className="inline-block object-cover object-center rounded-2xl"/>
        </div>
      </div>
      {/* Product List BestSale*/}
      {/* Pet List */}
    </>
  )
}
