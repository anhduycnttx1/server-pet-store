import { useDispatch } from 'react-redux'
import {} from './store'

let _dispatch: any | null
export function useHomePageController() {
  _dispatch = useDispatch()

  return {
    getExemple,
  }
}

const getExemple = async () => {
  console.log('Get Api Home Exemple')
}
