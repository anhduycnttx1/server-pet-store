import Head from 'next/head'
import React from 'react'
import AppLayout from '../../components/app-layout'

export default function PetPage() {
  return (
    <>
      <Head>
        <title>Pet List</title>
      </Head>
      <p>Pet List</p>
    </>
  )
}

PetPage.getLayout = (page: React.ReactElement) => {
  return <AppLayout>{page}</AppLayout>
}