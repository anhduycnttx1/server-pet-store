import Head from 'next/head'
import React from 'react'
import AppLayout from '../../components/app-layout'

export default function ProductPage() {
  return (
    <>
      <Head>
        <title>Propduct List</title>
      </Head>
      <p>Propduct List</p>
    </>
  )
}

ProductPage.getLayout = (page: React.ReactElement) => {
  return <AppLayout>{page}</AppLayout>
}