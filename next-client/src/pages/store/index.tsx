import Head from 'next/head'
import React from 'react'
import AppLayout from '../../components/app-layout'

export default function StorePage() {
  return (
    <>
      <Head>
        <title>Store List</title>
      </Head>
      <p>Store List</p>
    </>
  )
}

StorePage.getLayout = (page: React.ReactElement) => {
  return <AppLayout>{page}</AppLayout>
}