import Head from 'next/head'
import React from 'react'
import TheHeader from './TheHeader'
import TheFooter from './TheFooter'
import Link from 'next/link'

interface AppLayoutProps {
  showMenu?: boolean
  nav?: JSX.Element
  navWidth?: number | string
  searchBox?: JSX.Element
}

export default function AppLayout({children}: React.PropsWithChildren<AppLayoutProps>) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
      </Head>
      <main className="w-full min-w-[896px]">
        <TheHeader />
        {/* banner */}
        <div className="relative"> 
          <div className="transition duration-700 ease-in-out">
            <img src="/images/banner1.jpg" alt="Banner" className="h-96 w-full object-cover brightness-50 "/>
          </div>
          <div className=" absolute bottom-20 inset-x-0 text-center text-white flex justify-center">
            <div className="w-[440px]">
              <p className="my-7 text-center text-4xl shadow leading-snug">Thoả sức với đam mê thú cưng của bạn. </p>
              <Link href="/register">
                <a className="bg-[#fa8c16] px-8 py-2.5 rounded-lg text-lg font-medium shadow-md shadow-[#612500] hover:bg-[#ffa940] hover:text-white">
                  Đăng Ký Ngay
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="md:container mx-2 md:mx-auto">{children}</div>
        <TheFooter />
      </main>
    </>
  )
}
