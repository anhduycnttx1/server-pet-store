import React from 'react'

type TheFooterProps = {}
function TheFooter(props: TheFooterProps): React.ReactElement<TheFooterProps> {
  return (
    <>
      <div className="md:container mx-auto border-t mt-10">
          <div className="flex justify-between p-5">
            <div>© 2022 ShopPet. Tất cả các quyền được bảo lưu.</div>
            <div>Tổng đài hỗ trợ: 19009999 - Email: cskh@hotro.shoppet.vn</div>
          </div>
      </div>
    </>
  ) 
}

export default TheFooter
