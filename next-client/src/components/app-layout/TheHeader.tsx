import React from 'react'
import Image from 'next/image'
import {Menu} from 'antd'
import Link from 'next/link'
import {IconSearch, ShoppingCart} from '../IconCustom'

type TheHeaderProps = {}
function TheHeader(props: TheHeaderProps): React.ReactElement<TheHeaderProps> {
  const menuMockup = [
    {key: '0', title: 'Trang Chủ', urlNext: '/'},
    {key: '1', title: 'Thú Nuôi', urlNext: '/pet'},
    {key: '2', title: 'Sản phẩm', urlNext: '/product'},
    {key: '3', title: 'Của hàng', urlNext: '/store'},
    {key: '4', title: 'Về chúng tôi', urlNext: 'about'},
  ]
  return (
    <div className="shadow-sm border-b">
      <div className="md:container mx-auto">
        <div className="flex justify-between  xl:mx-2 h-16 items-center">
            <div className="transition duration-300 ease-in-out">
              <Link href='/'>
                <a>
                  <Image src="/images/logo.png" alt="logo app" width={40} height={40} />
                </a>
              </Link>  
            </div>
            <div className="font-medium w-1/3 flex-none ">
              <Menu mode="horizontal" defaultSelectedKeys={['0']} className="w-full" >
                {menuMockup.map((menu, index) => {
                  return (
                    <Menu.Item key={index}>
                      <Link href={menu.urlNext}>{menu.title}</Link>
                    </Menu.Item>
                  )
                })}
              </Menu>
            </div>
            <div className="flex justify-between items-center">
              <ShoppingCart className="w-5 h-5 block mr-5 hover:fill-gray-500" />
              <IconSearch className="w-5 h-5 block mr-5 hover:fill-gray-500" />
              <Link href="/login">
                <a><div className="bg-[#fa8c16] hover:bg-[#ffa940] hover:text-gray-50 px-5 py-2 text-white font-medium rounded-md">Đăng Nhập</div></a>
              </Link>
            </div>
        </div>
      </div>
    </div>
  )
}

export default TheHeader
