import  ProductItem  from '../product-item/index'
// import SlickContainer from 'common-ui/components/slick'
import {IconRefresh} from '../../IconCustom'
import Link from 'next/link'


type ProductCardProps = {
   header?: string
   viewMoreText?: string
   viewMoreUrl?: string
   loading? : boolean
//   slideToShow?: number
//   items: Models.ProductItemModel[]
}

export function ProductCard({
  header = '',
  viewMoreText = '',
  viewMoreUrl = '/',
  loading = true
//   slideToShow = null,
//   items = [],
}: React.PropsWithChildren<ProductCardProps>): React.ReactElement {
  const urlMock = [
    "https://www.vietpet.net/wp-content/uploads/2019/01/thuc-an-cho-cho-poodle-moshm.jpg",
    "https://toplist.vn/images/800px/dia-chi-ban-thuc-an-cho-meo-tai-ha-noi-166289.jpg",
    "https://www.vietpet.net/wp-content/uploads/2019/01/thuc-an-cho-cho-bulldog-moshm.jpg",
    "https://www.vietpet.net/wp-content/uploads/2019/01/thuc-an-cho-cho-bull-anh-dr-glint-expert-care.jpg",
  ]
  return (
    <>
      <div className="grid grid-cols-12 gap-4 mt-10">
        <div className="flex justify-between col-span-12 mb-5 item-content">
            {header && <h3 className="text-3xl font-bold text-[#fa8c16]">{header}</h3>}
            {viewMoreText && (
            <Link href={viewMoreUrl}>
                <a className="flex font-medium rounded-md items-center hover:bg-[#ffa940] bg-[#fa8c16] px-3 py-1 text-white hover:text-white">
                <span className="">{viewMoreText}</span>
                <IconRefresh className={`h-4 w-4 ml-2 fill-white stroke-2 ${loading? 'animate-spin':''}`} />
                </a>
            </Link>
            )}
        </div>
        {
          new Array(12).fill(null).map((_, index) => {
            return (
              <ProductItem key={index} redirectUrl={urlMock[index % 4]}/>
            )
          })
        }
       
      </div> 
    </>
  )
}
