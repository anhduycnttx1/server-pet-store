import Link from 'next/link'
import { useState } from 'react'
import { IconHeartBlack, IconHeartWhite } from '../../IconCustom'
import TruncateSting from '../../../common/helpers/truncateString'
type ProducerItemProps = {
   redirectUrl?: string
   item?: any
   noMargin?: boolean
}

export default function ProducerItem({redirectUrl = '/', item = {}, noMargin, 
}: React.PropsWithChildren<ProducerItemProps>): JSX.Element {
  const [isFollower, setIsFollower] = useState(false)
  const productTitle = "Beneful Healthy Radiance - Beneful Healthy Radiance"
  return (
    <> 
      <div className="relative col-span-4 border shadow-md md:col-span-3 xl:col-span-2 hover:border-[#fa8c16] hover:-top-2">
        <div className="relative">
          <div className="absolute z-10 rounded-full bottom-1 left-3">
          <button onClick={()=>setIsFollower(!isFollower)}>
              { isFollower? 
              <IconHeartBlack className="w-5 h-5 fill-rose-400 hover:fill-rose-600"/> 
              : 
              <IconHeartWhite className="w-5 h-5 fill-slate-300 hover:fill-rose-300"/> 
              }   
            </button>  
          </div>
          <Link href={'item.producerUrl'}>
            <a className="hover:text-[#ffa940]">
              {/* product-image */}
              <div className="relative font-medium text-white">
                <img src={redirectUrl}  
                  alt={'item.producerName'} 
                  className="object-cover object-center"/>
                <span className="hover:bg-[#ffa940] bg-[#fa8c16] px-2 py-0.5 text-lg md:text-base ordinal absolute -bottom-1 -right-2">
                  {'18,000'} đ
                </span>
              </div>
              {/*producer-info*/}
              <div className="flex flex-col justify-between h-20 px-2 py-1">
                <span className="break-all whitespace-normal">{TruncateSting(productTitle,48)}</span>
                <div className="mx-1 mb-1 text-right text-gray-500">Còn lại {100}</div>
              </div>
            </a>
          </Link>
        </div>

      </div>
    </>
  )
}

