
export default function TruncateSting(str: string, num: number): string {
    return (str.length > num) ? str.slice(0, num - 1) + '...' : str;
}