import { Controller, Delete, Get, Param, Put } from '@nestjs/common'

@Controller('/api/v1/pets')
export class PetsController {
  @Get(':id')
  getDetailPet(@Param('id') id: string): string {
    return `this is page detail pet id: ${id}`
  }

  @Delete(':id')
  deletePet(@Param() id: string): string {
    return `this is page delete pet id: ${id}`
  }

  @Put(':id')
  putPet(@Param('id') id: string): string {
    return `this is page put pet id: ${id}`
  }
}
