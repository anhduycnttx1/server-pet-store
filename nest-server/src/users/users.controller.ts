import {Body, Controller, Delete, Get, Param, Post, Res} from '@nestjs/common'
import {UsersService} from './users.service'
import {User as UserModel} from '@prisma/client'
import * as argon2 from 'argon2'
import * as jwt from 'jsonwebtoken'
import {Response} from 'express'

@Controller('api/v1/auth')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('users')
  async getUsers(): Promise<UserModel[]> {
    return this.usersService.users({})
  }

  @Post('register')
  async regidterUser(
    @Body() userData: {username: string; email: string; password: string},
    @Res() res: Response
  ): Promise<any> {
    const {username, email, password} = userData
    if (!username || !email || !password)
      return res.status(400).json({statusCode: 400, message: 'Missing Usename or Email or Password'})
    const user = await this.usersService.findUserWhere({OR: [{username: username}, {email: email}]})
    if (user) return res.status(400).json({statusCode: 400, message: 'Usename or Email already exists'})
    const hashPassword = await argon2.hash(password)
    const userNew = await this.usersService.createUser({username, email, password: hashPassword})
    return res.status(201).json({statusCode: 201, message: `Success create user id: ${userNew.id}`})
  }

  @Post('login')
  async loginUser(@Body() data: {username: string; password: string}, @Res() res: Response): Promise<any> {
    const {username, password} = data
    if (!username || !password) return res.status(400).json({statusCode: 400, message: 'Missing Usename or Password'})
    const user = await this.usersService.findUserUnique({username})
    if (!user) return res.status(400).json({statusCode: 400, message: 'Usename or Email already exists'})
    const passwordVerify = await argon2.verify(user.password, password)
    if (!passwordVerify) return res.status(400).json({statusCode: 400, message: 'Password already exists'})
    const payload = {
      iat: Date.now(),
      exp: Date.now() + 3600 * 12,
      user_id: user.id
    }
    const token_access = jwt.sign(payload, 'anhduy1402')
    return res.status(200).json({statusCode: 200, message: 'Success Login', token_access})
  }

  @Get('users/:id')
  async getUserById(@Param('id') id: string, @Res() res: Response): Promise<any> {
    const user = await this.usersService.findUserUnique({id: id})
    if (!user) return res.status(404).json({statusCode: 404, message: 'User not found!'})
    return res.status(200).json(user)
  }

  @Delete('user/:id')
  async deleteUserById(@Param('id') id: string, @Res() res: Response): Promise<any> {
    const user = await this.usersService.findUserUnique({id: id})
    if (!user) return res.status(404).json({statusCode: 404, message: 'User not found!'})
    const delUser = await this.usersService.deleteUser({id: id})
    return res.status(202).json({statusCode: 200, message: `Success delete user id: ${delUser.id}`})
  }
}
