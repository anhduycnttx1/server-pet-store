import {Injectable} from '@nestjs/common'
import {PrismaService} from '../prisma.service'
import {User as UserModel, Prisma} from '@prisma/client'

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async findUserWhere(
    userScalarWhereWithAggregatesInput: Prisma.UserScalarWhereWithAggregatesInput
  ): Promise<UserModel | null> {
    return this.prisma.user.findFirst({
      where: userScalarWhereWithAggregatesInput
    })
  }
  async findUserUnique(userWhereUniqueInput: Prisma.UserWhereUniqueInput): Promise<UserModel | null> {
    return this.prisma.user.findUnique({
      where: userWhereUniqueInput
    })
  }

  async users(params: {
    skip?: number
    take?: number
    cursor?: Prisma.UserWhereUniqueInput
    where?: Prisma.UserWhereInput
    orderBy?: Prisma.UserOrderByWithRelationInput
  }): Promise<UserModel[]> {
    const {skip, take, cursor, where, orderBy} = params
    return this.prisma.user.findMany({skip, take, cursor, where, orderBy})
  }

  async createUser(data: Prisma.UserCreateInput): Promise<UserModel> {
    return this.prisma.user.create({data})
  }

  async deleteUser(where: Prisma.UserWhereUniqueInput): Promise<any> {
    return this.prisma.user.delete({
      where
    })
  }
}
